// CPU frequency in HZ
#define F_CPU 8000000

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// Servo timings in usecs
// PERIOD - Length of one period of servo contol signal, usually 20ms
// MIN_PULSE - Pulse length for servos minimal position, usually 1ms
// MAX_PULSE - Pulse length for servos maximal position, usually 2ms
#define PERIOD 20000
#define MIN_PULSE 1000
#define NETURAL_PULSE 1500
#define MAX_PULSE 2000

// Test modes
#define TEST_CENTER                                                 0x00
#define TEST_UTTERMOST_SWING                                        0x01
#define TEST_UTTERMOST_SWING_DELAY                                  50

#define TEST_SLOW_SWING                                             0x02
#define TEST_SLOW_SWING_INCREMENT                                   4

#define TEST_FAST_JUMPS                                             0x03
uint16_t jump_coords[] = {
    1000,  1800,   1200,   1500,   1100,   1150,   1300,   2000,   1200,   1520
};
uint8_t jump_delays[] = {
    16,    12,      6,     8,      2,      4,      14,     16,     6,      10
};



////////////////////////////////////////////////////////////////////////////////
// Servo control interrupt
// In usecs
////////////////////////////////////////////////////////////////////////////////
// Currently running test
uint8_t current_job = 0;
uint16_t current_pulse_width = NETURAL_PULSE;
// Current delay counter till next calculation in PERIODs (20ms bits)
uint8_t delay = TEST_UTTERMOST_SWING_DELAY;
uint8_t fj_pointer = 0;
int8_t increment = TEST_SLOW_SWING_INCREMENT;


void _calculate_next_pulsewidth(void)
{
    if ( delay == 0 )
    {
        switch ( current_job )
        {
            case TEST_CENTER:
                current_pulse_width = NETURAL_PULSE;
                delay = 5;
            break;
            case TEST_UTTERMOST_SWING:
                // Toggle min/max pulse width
                if ( current_pulse_width == MAX_PULSE )
                {
                    current_pulse_width = MIN_PULSE;
                }
                else
                {
                    current_pulse_width = MAX_PULSE;
                }

                // Set next toggle delay
                delay = TEST_UTTERMOST_SWING_DELAY;
            break;
            case TEST_SLOW_SWING:
                if ( current_pulse_width >= MAX_PULSE )
                {
                    increment = -TEST_SLOW_SWING_INCREMENT;
                }
                else if ( current_pulse_width <= MIN_PULSE )
                {
                    increment = TEST_SLOW_SWING_INCREMENT;
                }

                // Increment PW
                current_pulse_width += increment;

                // Set next toggle delay
                delay = 1;
            break;
            case TEST_FAST_JUMPS:
                current_pulse_width = jump_coords[fj_pointer];
                delay = jump_delays[fj_pointer];
                fj_pointer++;

                if ( fj_pointer > 9 ) fj_pointer = 0; 
            break;
            default:
                current_pulse_width = NETURAL_PULSE;
                delay = 50;
        }
    }
    else delay--;
}

ISR(TIMER1_OVF_vect)
{
    cli();
    if ( PORTC & 0x01 )
    {
        // PORTC is active, reset it and set low time
        PORTC = 0x00;
        TCNT1=65535-PERIOD+current_pulse_width;
        
        // Calculate pulsewidth for next cycle while we have some time (~18ms)
        _calculate_next_pulsewidth();
    }
    else
    {
        // PORTC is inactive, activate it on high time
        PORTC = 0x01;
        TCNT1=65535-current_pulse_width;
    }
    sei();
}

////////////////////////////////////////////////////////////////////////////////
// Initialise IO pins and timers
void init()
{
    // PORTB used only for ISP, disable it
    // pullup is set to reduce flapping power consumption
    DDRB|=0x00;
    PORTB=0xFF;

    // PORTC - output for servo
    DDRC=0x01;
    PORTC=0xFF;

    // PORTD - output for LCD
    DDRD=0xFF;
    PORTD=0x00;

    // Timer 1 - Toggles servo output pins
    // Set timer scaler F/8
    TCCR1B = 0x02;
    // F/1024 (8kHz - 125us)
    TCCR0 = 0x05;

    // Enable Overflow Interrupt
    TIMSK|=(1<<TOIE1);
    TIMSK|=(1<<TOIE0);

    // Zero timers
    TCNT1=65535-PERIOD;
    TCNT0=255;

    // Enable interrups globally
    sei();
}

////////////////////////////////////////////////////////////////////////////////

int main()
{
    // Init
    init();

    // Main loop
    while(1);
}


////////////////////////////////////////////////////////////////////////////////
// Button anti-flap interrupt
////////////////////////////////////////////////////////////////////////////////
// Each cycle ~30ms
#define PRESSED_CYCLES 3
int8_t presed_cnt = 0;
ISR(TIMER0_OVF_vect)
{
    // No previous press detected
    if ( presed_cnt < PRESSED_CYCLES )
    {
        if ( (PINB & 0x01) == 0 ) presed_cnt++;
        else presed_cnt = 0;
    }
    else if ( presed_cnt >= PRESSED_CYCLES )
    {
        if ( (PINB & 0x01) == 0 ) {
            presed_cnt = -20;
            current_job++;
            if ( current_job > 3 ) current_job = 0;
        }
        else presed_cnt = 0;
    }
    // Timer overflow in 30ms
    TCNT0=15;
}


