TARGET = servo-tester
MCU = atmega8
PROGRAMMER = usbasp
BRANCH = debug

CFLAGS = -Wall -Os -std=gnu99

################################################################################

CC = /usr/bin/avr-gcc
OBJCOPY = /usr/bin/avr-objcopy
SRCDIR = src
BINDIR := bin/$(BRANCH)

################################################################################

.PHONY: all
all: $(BINDIR)/$(TARGET).hex

$(BINDIR):
	mkdir -p $(BINDIR)

$(BINDIR)/%.o: $(SRCDIR)/%.c $(BINDIR)
	$(CC) $(CFLAGS) -mmcu=$(MCU) $< -o $@


$(BINDIR)/$(TARGET).hex: $(BINDIR)/main.o 
	$(OBJCOPY) -O ihex $< $@

.PHONY: clean
clean:
	rm -rf $(BINDIR)

.PHONY: flash
flash: clean $(BINDIR)/$(TARGET).hex
	sudo avrdude -c $(PROGRAMMER) -U flash:w:$(BINDIR)/$(TARGET).hex -p $(MCU)

.PHONY: reset
reset:
	sudo avrdude -c $(PROGRAMMER) -p $(MCU) 
